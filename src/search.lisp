(uiop:define-package #:cache-cache.search
    (:documentation "Interface and utilities for searching.")
  (:use #:cl)
  (:use-reexport #:cache-cache.generic)
  (:export
   #:search-in-list
   #:search-result
   #:find-by))

(in-package #:cache-cache.search)

(defun search-in-list (needle list
                       &key
                         (key #'identity)
                         (test #'string-equal))
  "Return the items that has NEEDLE in KEY."
  (remove-if-not
   #'(lambda (item &aux (haystack (funcall key item)))
       (when haystack
         (search needle haystack :test test)))
   list))

(defun search-in-list/and (needle-list list
                           &key
                             (key #'identity)
                             (test #'string-equal))
  "Return the items that has all needles from NEEDLE-LIST in KEY."
  (loop
    :for needle :in needle-list
    :for candidates = (search-in-list needle list :key key :test test)
      :then (search-in-list needle candidates :key key :test test)
    :finally (return candidates)))

(defclass search-result ()
  ((source
    :initform (error ":source must be specified")
    :initarg :source
    :reader source
    :documentation "The source in which the result can be found.")
   (id
    :initform (error ":id must be specified")
    :initarg :id
    :reader id
    :documentation "The id of the result, this id is source-specific."))
  (:documentation "Represents one item in a search-result"))

(defun find-by-brute-force (query source topic key)
  "Return the TOPIC in SOURCE that contains all the parts of QUERY in their KEY."
  (when (items source topic)
    (search-in-list/and
     (split-sequence:split-sequence #\Space query :remove-empty-subseqs t)
     ;; TODO avoid consing a list of values only to filter it right away...
     (alexandria:hash-table-values (items source topic))
     :key key)))


;;; Indexing

(defun ngrams (n string)
  "Generate all N-grams of STRING, the list is not deduplicated."
  (loop :for i :below (- (length string) 3)
        :for ngram = (make-array
                      n
                      :element-type (array-element-type string)
                      :displaced-to string
                      :displaced-index-offset i)
        :collect ngram))

(defun normalized-ngrams (n string)
  (if (= n (length string))
      (list string)
      (remove-duplicates
       (mapcar #'string-downcase
               (ngrams n string))
       :test #'string=)))

;; TODO Maybe add a timestamp/checkpoint??
(defclass index ()
  ((table
    :initform nil
    :initarg :table
    :accessor table)
   (source
    :initform nil
    :initarg :source
    :accessor source)
   (topic
    :initform nil
    :initarg :topic
    :accessor topic)
   (key
    :initform nil
    :initarg :key
    :accessor key))
  (:documentation "A hash-table with some metadata."))

(defun build-index (source topic key)
  (let ((index (make-hash-table :test #'equal))
        (items (items source topic)))
    (when items
      (maphash
       #'(lambda (id item)
           (loop :for ngram :in (normalized-ngrams 3 (funcall key item))
                 :do (pushnew id (gethash ngram index))))
       items))
    (make-instance
     'index
     :table index
     :source source
     :topic topic
     :key key)))

;; (normalized-ngrams 3 "dep")

(defun reduce-index (table ngrams reducer &key early-exit-p accumulator)
  (loop
    :for ngram :in ngrams
    :do
       (setf accumulator
             (if accumulator
                 ;; Update accumulator
                 (funcall reducer
                          accumulator
                          (gethash ngram table))
                 ;; Initialize accumulator
                 (gethash ngram table)))
    :while (or (not early-exit-p) accumulator)
    :finally (return accumulator)))

(defun narrow-down-using-index (index query &optional candidates)
  (let ((table (table index)))
    (if (< (length query) 3)
        (reduce-index table
                      (search-in-list query (alexandria:hash-table-keys table))
                      #'union)
        #++ (loop :for ngram :in (search-in-list query (alexandria:hash-table-keys table))
                  :append (gethash ngram table))
        (reduce-index table (normalized-ngrams 3 query) #'intersection)
        #++
        (loop
          :for ngram :in (normalized-ngrams 3 query)
          :do
             (setf candidates
                   (if candidates
                       ;; Update candidates
                       (intersection
                        candidates
                        (gethash ngram table))
                       ;; Initialize candidates
                       (gethash ngram table)))
             ;; No candidates left, early exit
          :while candidates
          :finally (return candidates)))))

(defun find-using-index (index query)
  (let ((source (source index))
        (topic (topic index))
        (key (key index)))
    (search-in-list
     query
     (mapcar
      (lambda (id)
        (item source topic id))
      (narrow-down-using-index index query))
     :key key)))

#++
(defun applicable (index source topic key)
  (and (eq source (source index))
       (eq topic (topic index))
       (eq key (key index))))

(defparameter *indexes* (make-hash-table :test 'equal)
  "Set of all indexes.")

;; Creating an index on issues' title
#++
(let ((index-designator
        (list (cache-cache::source-by-id 1) :issue #'cache-cache.gitlab.search::item-title)))
  (setf (gethash index-designator *indexes*)
        (apply #'build-index index-designator)))

(defun find-by (query source topic key)
  "Return the TOPIC in SOURCE that contains all the parts of QUERY in their KEY."
  (let* ((index-designator (list source topic key))
         (index (gethash index-designator *indexes*)))
    (if index
        (find-using-index index query)
        (find-by-brute-force query source topic key))))
