(uiop:define-package #:cache-cache.quicklisp
    (:documentation "Quicklisp distribution as a data source.")
  (:use #:cl)
  (:use-reexport #:cache-cache.generic))

(in-package #:cache-cache.quicklisp)

(defclass quicklisp-source (source)
  ((distribution
    :initform (error ":distribution must be specified")
    :initarg :distribution
    :accessor distribution
    :documentation "The quicklisp distribution to cache."))
  (:documentation "A data source from a quicklisp distribution."))

;; Doh! I use guix for package management... So I don't have quicklisp
;; installed... but I wanted to try cache-cache on quicklisp to have
;; some kind of fancy search :/
