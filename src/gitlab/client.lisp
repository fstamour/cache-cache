(uiop:define-package #:cache-cache.gitlab.client
    (:documentation "Utilities to make requests to GitLab")
  (:use #:cl)
  (:use-reexport
   #:cache-cache.generic
   #:cache-cache.source
   #:cache-cache.gitlab.source)
  (:local-nicknames (#:a #:alexandria)
                    (#:lt #:local-time)
                    (#:jzon #:com.inuoe.jzon)))

(in-package #:cache-cache.gitlab.client)


;;; Find latest object in hash-table

(defun find-latest (objects field)
  "Given a hash-table of OBJECTS, find the FIELD with the latest time."
  (loop
    :with latest = nil
    :for id :being :the :hash-key :of objects :using (hash-value object)
    :for current = (lt:parse-timestring (gethash field object))
    :if (null latest)
      :do (setf latest current)
    :else
      :do (when (lt:timestamp< latest current)
            (setf latest current))
    :finally (return latest)))

(defun find-last-update-time (issues)
  "Given a hash-table of ISSUES, find the lastest update-time."
  (find-latest issues "updated_at"))

#+ (or)
(time
 (find-last-update-time (items (source-by-id 1) :issue)))



;;; Common getters

;; TODO move to utilities
(defun dig (keys hash-table)
  (loop
    :for key :in keys
    :while (and hash-table
                (hash-table-p hash-table))
    :do (setf hash-table (gethash key hash-table)))
  hash-table)

#++
(progn
  (dig '(a 2) nil)
  (dig '(:a) (a:plist-hash-table '(:a 2)))
  (dig '(:a :b) (a:plist-hash-table `(:a ,(a:plist-hash-table '(:b 3))))))



(macrolet ((getter (name
                    &aux
                      (getter (a:symbolicate '#:item- name))
                      (key (string-downcase (substitute #\_ #\- (symbol-name name)))))
             `(progn
                ;; Getter
                (defun ,getter (item)
                  ,(format nil "Return the ITEM's \"~a\" property." name)
                  (multiple-value-bind (value present-p)
                      (dig ',(split-sequence:split-sequence #\/ key) item)
                    (values (if (eq 'null value) nil value)
                            present-p)))
                ;; Membership predicate
                (defun ,(a:symbolicate '#:item- '#:has- name '#:-p) (item)
                  ,(format nil "Return true if the ITEM has a \"~a\" attribute." name)
                  (multiple-value-bind (_ present-p) (,getter item)
                    (declare (ignore _))
                    present-p))
                ;; Membership predicate + Null predicate
                (defun ,(a:symbolicate '#:item- name '#:-p) (item)
                  ,(format nil "Return true if the ITEM has a \"~a\" and is not 'null." name)
                  (multiple-value-bind (value present-p) (,getter item)
                    (and present-p (not (eq 'null value))))))))
  (getter :-links)
  (getter :assignee)
  (getter :assignees)
  (getter :author)
  (getter :blocking-issues-count)
  (getter :closed-at)
  (getter :closed-by)
  (getter :confidential)
  (getter :created-at)
  (getter :description)
  (getter :discussion-locked)
  (getter :downvotes)
  (getter :due-date)
  (getter :epic)
  (getter :epic-iid)
  (getter :has-tasks)
  (getter :id)
  (getter :iid)
  (getter :issue-type)
  (getter :labels)
  (getter :merge-requests-count)
  (getter :milestone)
  (getter :moved-to-id)
  (getter :name)
  (getter :name-with-namespace)
  (getter :path)
  (getter :path-with-namespace)
  (getter :project-id)
  (getter :references)
  (getter :references/full)
  (getter :service-desk-reply-to)
  (getter :state)
  (getter :task-completion-status)
  (getter :task-status)
  (getter :time-stats)
  (getter :title)
  (getter :type)
  (getter :updated-at)
  (getter :upvotes)
  (getter :user-notes-count)
  (getter :web-url)
  (getter :weight))

(defun item-closed-p (issue)
  (or (item-closed-at-p issue)
      (equal "closed" (item-state issue))))




;;; TODO Handle rate-limiting gracefully https://docs.gitlab.com/ee/user/admin_area/settings/user_and_ip_rate_limits.html#response-headers
;;; e.g.
;;; - use some kind of queues (maybe chanl?)
;;; - look at headers Rate-Limit-Remaining and RetryAfter
(defun http-request-gitlab (uri token &rest rest)
  (log:debug "Making a request to GitLab: \"~a\"..." uri)
  ;; TODO Move that handler-case somewhere else, e.g. in the labmda
  ;; ran by cl-cron.
  (handler-case
      (multiple-value-bind
            (body status-code headers uri-object stream must-close reason-phrase)
          (apply #'drakma:http-request
                 (ensure-uri-string uri)
                 ;; TODO Parse "rest" to extract ":additional-headers"
                 ;; Send the auth
                 :additional-headers (list (token-header token))
                 (alexandria:remove-from-plist rest
                                               :additional-headers))
        (declare (ignore stream must-close uri-object))
        (declare (ignorable headers))
        ;; status-code reason-phrase
        (log:debug "~A ~A" status-code reason-phrase)
        (setf *last-headers* headers
              *last-body* body)
        ;; TODO only parse as json if the response's content-type is application/json
        (let ((response (jzon:parse body)))
          (a:when-let ((message (errorp response)))
            ;; TODO better error message
            ;; TODO specific condition...
            ;; TODO add a restart
            (error "http-request REST error: message = ~a (~a)" message
                   (jzon:stringify response)))
          (values response headers)))
    #++
    (error (condition)
      (break)
      (log:error "~a" condition)
      nil)))
