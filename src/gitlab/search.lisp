(defpackage #:cache-cache.gitlab.search
  (:documentation "Interface to search the local cache for GitLab sources")
  (:use #:cl #:cache-cache.gitlab.source)
  (:local-nicknames (#:a #:alexandria)
                    (#:lt #:local-time))
  (:import-from #:com.inuoe.jzon
                #:write-value*
                #:write-object*)
  (:import-from #:cache-cache.gitlab.client
                #:item-title
                #:item-name
                #:item-name-with-namespace
                #:item-path-with-namespace
                #:item-web-url
                #:item-created-at
                #:item-updated-at
                #:item-closed-at-p
                #:item-closed-p
                #:item-references/full)
  (:import-from #:cache-cache.search
                #:find-by)
  (:export
   #:find-issues
   #:find-projects))

(in-package #:cache-cache.gitlab.search)

(defun find-issues (query source)
  "Return the issues that contains all the parts of QUERY in their title."
  (find-by query source :issue #'item-title))

(defun find-projects (query source)
  "Return the projects that contains all the parts of QUERY in their \"name with namespace\"."
  (find-by query source :project #'item-name-with-namespace))

(defun find-epics (query source)
  "Return the issues that contains all the parts of QUERY in their title."
  (find-by query source :epic #'item-title))


;; TODO utils
(defun timestamp-string< (a b &optional c)
  (and
   (lt:timestamp<
    (lt:parse-rfc3339-timestring a)
    (lt:parse-rfc3339-timestring b))
   (or (not c)
       (timestamp-string< b c))))

;; TODO utils
(defun timestamp-string> (a b)
  (lt:timestamp>
   (lt:parse-rfc3339-timestring a)
   (lt:parse-rfc3339-timestring b)))

;; TODO rename to issue<
(defun compare-issues (issue1 issue2)
  "Should ISSUE1 be shown before ISSUE2?"
  (let ((closed1 (item-closed-at-p issue1))
        (closed2 (item-closed-at-p issue2)))
    (if (eq closed1 closed2)
        (timestamp-string>
         (item-updated-at issue1)
         (item-updated-at issue2))
        closed2)))

(defun issues-created-in-the-last-7-days (source)
  (let ((last-week (lt:adjust-timestamp
                       (lt:today)
                     (offset :day -7))))
    (when (items source :issue)
      (remove-if #'(lambda (issue)
                     (lt:timestamp<
                      (lt:parse-rfc3339-timestring (item-created-at issue))
                      last-week))
                 (a:hash-table-values (items source :issue))))))


;; Testing find-issues
#+ (or)
(time
 (let ((query "auto update"))
   (format t "~&=================================")
   (mapcar #'item-title
           (find-issues query
                        (source-by-id 1)))))

(defun sort-by-score (query list)
  (stable-sort
   list
   (lambda (a b &aux
                  (a= (string= query a))
                  (b= (string= query b))
                  (in-a-p (search query a))
                  (in-b-p (search query b)))
     (cond
       ;; exact matches first
       ((and a= (not b=)) t)
       ((and (not a=) b=) nil)
       ;; if they both match... keep the same order
       ((and a= b=) t)
       ;; if the query is found exactly, sort first
       ((and in-a-p (not in-b-p)) t)
       ((and (not in-a-p) in-b-p) nil)
       ;; TODO else: sort the "best match" (longest?) first
       (t (string< a b))))
   :key #'item-name))

(defun write-item (type item &key text url ref extras)
  (apply #'write-object*
         "type" type
         "id"  (id item)
         "text" (or text (item-title item))
         "url" (or url (item-web-url item))
         "ref" (or ref (item-references/full item))
         extras))

(defun hack/search (query source callback &optional topics)
  (time
   (if (str:non-blank-string-p query)
       (flet ((include-topic-p (topic)
                (or (null topics) (member topic topics :test 'eq))))
         ;; Add projects
         (when (include-topic-p :project)
           (let ((projects (sort-by-score
                            query
                            (find-projects query source))))
             (loop :for project :in projects
                   :do (funcall callback :project project))))
         ;; Add epics
         (when (include-topic-p :epic)
           ;; TODO create function "item-closed-p" and "item<"
           (loop
             :for epic :in (sort (find-epics query source) #'compare-issues)
             :do (funcall callback :epic epic)))
         ;; Add issues
         (when (include-topic-p :issue)
           (let* ((issues (sort (find-issues query source) #'compare-issues))
                  (length (length issues)))
             (loop
               :for issue :in issues
               :do (funcall callback :issue issue)))))
       ;; When query is empty
       (loop
         :for issue :in (sort (issues-created-in-the-last-7-days source) #'compare-issues)
         :do (funcall callback :issue issue)))))

;; TODO maybe "query" should be a more complex object (e.g. include the topics)?
(defun handler/search (query source &optional topics)
  (time
   (if (str:non-blank-string-p query)
       (flet ((include-topic-p (topic)
                (or (null topics) (member topic topics :test 'eq))))
         ;; Add projects
         (when (include-topic-p :project)
           (let ((projects (sort-by-score
                            query
                            (find-projects query source))))
             (loop :for project :in projects
                   :do (write-item "> project" project
                                   :text (item-path-with-namespace project)
                                   #++ (item-name-with-namespace project)
                                   :url (format nil "~a/issues" (item-web-url project))
                                   :ref (format nil "~a>" (item-name-with-namespace project))))))
         ;; Add epics
         (when (include-topic-p :epic)
           ;; TODO create function "item-closed-p" and "item<"
           (loop
             :for epic :in (sort (find-epics query source) #'compare-issues)
             :do (write-item "& epic" epic
                             :extras (list "closed" (item-closed-p epic)))))
         ;; Add issues
         (when (include-topic-p :issue)
           (let* ((issues (sort (find-issues query source) #'compare-issues))
                  (length (length issues)))
             (loop
               :for issue :in issues
               :for i :below 100 ; no more than a hundred issues
               :do (write-item "# issue" issue
                               :extras (list "closed" (item-closed-p issue))))
             (when (< 100 length)
               (write-object*
                :type "comment"
                :text
                (format nil "There were ~D matching issues total, only 100 are shown." length))))))
       ;; When query is empty
       (loop
         :for issue :in (sort (issues-created-in-the-last-7-days source) #'compare-issues)
         :do (write-item "# issue" issue
                         :extras (list "closed" (item-closed-p issue)))))))

(defmethod search-source ((source gitlab-group-source) query &key topics &allow-other-keys)
  "Search through the GitLab group for QUERY."
  (handler/search query source topics))
