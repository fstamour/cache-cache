(in-package #:cache-cache.gitlab.client)


;;; Epics

(defun get-all-epics (source &optional callback)
  (http-request-get-all
   (format nil
           "~a/groups/~a/epics?per_page=1000"
           (api-url source)
           (group-id source))
   (token source)
   callback))

;; TODO get-new-and-updated-epics

(defmethod initialize-topic ((source gitlab-group-source)
                             (topic (eql :epic)))
  (refresh-topic source topic)
  #++
  (cache-cache.task:define-recurring-task
      (`((,id source) refresh ,topic) :run-now-p t)
    (refresh-topic source topic)))

(defmethod refresh-topic ((source gitlab-group-source)
                          (topic (eql :epic)))
  #++ (if (items source topic)
          (progn
            (log:info "Updating the list of epics from GitLab...")
            ;; TODO export/import cache-cache::by-id
            (cache-cache::by-id (get-new-and-updated-epics source)
                                (items source topic))))
  (progn
    (log:info "Getting all the epics from GitLab...")
    (setf (items source topic) (cache-cache::by-id (get-all-epics source)))
    (log:info "Got all the epics.")))


#++
(refresh-topic (source-by-id 1) :epic)
#++
(topics (source-by-id 1))

;; (items (source-by-id 1) :epic)
