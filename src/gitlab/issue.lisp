(in-package #:cache-cache.gitlab.client)


;;; Issues

(defun issue-uri (source &rest query-parameters &key project-id issue-iid &allow-other-keys)
  (cache-cache::make-uri
   (api-url source)
   "projects/"
   (cache-cache::ensure/ project-id)
   "issues/"
   issue-iid
   (apply #'cache-cache::format-query
          (a:remove-from-plist query-parameters
                               :project-id
                               :issue-iid))))

(defun remove-moved (items)
  "Remove issues that were moved"
  (remove-if #'item-moved-to-id items))

(defun issue-by-id (source id)
  (item source :issue id))

(defun issue-project (source issue-id)
  (project-by-id source
                 (issue-project-id (issue-by-id source issue-id))))



(defun get-all-issues (source &optional callback)
  (remove-moved
   (http-request-get-all
    (format nil
            "~a/groups/~a/issues?per_page=100"
            (api-url source)
            (group-id source))
    (token source)
    callback)))

(defun get-new-and-updated-issues (source)
  (let* ((latest-time
           ;; TODO make a generic method (last-checkpoint source
           ;; topic)
           (find-last-update-time (items source :issue)))
         (new-and-updated-issues
           (if latest-time
               (http-request-get-all
                (format
                 nil
                 "~a/groups/~a/issues?per_page=100&updated_after=~a"
                 (api-url source)
                 (group-id source)
                 (lt:format-rfc3339-timestring
                  nil
                  (lt:adjust-timestamp latest-time (offset :sec 1))))
                (token source))
               (get-all-issues source))))
    (log4cl:log-info (length new-and-updated-issues))
    new-and-updated-issues))

;; TODO -> initialize (issues)
(defmethod initialize-topic ((source gitlab-group-source)
                             (topic (eql :issue)))
  (cache-cache.task:define-recurring-task
      (`(,(id source) refresh ,topic) :run-now-p t)
    (refresh-topic source topic)))

;; TODO see workbench for: get-all-issues, but "incremental"
(defmethod refresh-topic ((source gitlab-group-source)
                          (topic (eql :issue)))
  (if (and (items source topic)
           (plusp (hash-table-count (items source topic))))
      (progn
        (log:info "Updating the list of issues from GitLab...")
        ;; TODO export/import cache-cache::by-id
        (cache-cache::by-id (get-new-and-updated-issues source)
                            (items source topic)))
      (progn
        (log:info "Getting all the issues from GitLab...")
        (setf (items source topic) (cache-cache::by-id (get-all-issues source)))
        (log:info "Got all the issues."))))



;; Should this go in "search.lisp"?
(defun issues-by-label (source &optional (destination (make-hash-table :test 'equal)))
  (loop
    ;; iterate over all issues
    :for iid
      :being :the hash-key :of (items source :issue)
        :using (hash-value issue)
    :do (map nil
             (lambda (label)
               (pushnew iid (gethash label destination (list))))
             (item-labels issue)))
  destination)
