(in-package #:cache-cache.gitlab.source)

(defclass gitlab-group-source (gitlab-source)
  ((id
    :initform (error "A group id must be specified.")
    :initarg :id
    :accessor id
    :documentation "The id of the GitLab group.")
   ;; GraphQl queries requires the full-path of a group to be able to
   ;; query it.
   (full-path
    :initform (error "A group full-path must be specified.")
    :initarg :full-path
    :accessor full-path
    :documentation "The full-path of the GitLab group."))
  (:documentation "A specific GitLab group."))

(defmethod supported-topics append ((source gitlab-group-source))
  (list :project :issue :epic))

#++
(make-instance 'gitlab-group-source
               :id 42
               :source-id -1
               :name "test"
               :instance *test-instance*)

(defmethod group-id ((source gitlab-group-source))
  (id source))

#++ ;; Fetching a group
(let ((source (source-by-id 1)))
  (cache-cache.gitlab.client::http-request-gitlab
   (format nil
           "~a/groups/~a"
           (api-url source)
           (group-id source))
   (token source)))
