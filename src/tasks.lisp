(uiop:define-package #:cache-cache.task
    (:documentation "Generic task management")
  (:use #:cl)
  (:use-reexport #:cache-cache.generic)
  (:export
   #:register-recurring-task
   #:unregister-recurring-task
   #:define-recurring-task))

(in-package #:cache-cache.task)

;; register-task: a wrapper on cl-cron:make-cron-job
;; - [x] add error handling
;; - [ ] add logging
;; - [ ] add metrics
;; - [ ] perhaps more? breakers?
;; - [ ] save the last time the task was executed
;; - [ ] use the "last time" to avoid running some tasks too often
;; - [ ] save streams (e.g. to get the logs, they could be shown in the web ui)



;; TODO add a lock on *tasks*
(defvar *tasks* (make-hash-table :test 'equal))

(defclass task ()
  ((id
    :initform (error ":id must be specified")
    :initarg :id
    :accessor id
    :documentation "Something to identify this task.")
   (handler
    ;; :initform (error ":id must be specified")
    :initarg :handler
    :accessor handler
    :type (or symbol function)
    :documentation "The function that implements this task.")
   (lock
    :accessor lock
    :documentation "A mutex to prevent concurrent execution of the same task.")))

(defmethod print-object ((task task) stream)
  (print-unreadable-object (task stream :type t :identity t)
    (format stream "~a ~a ~a"
            (id task)
            (lock task)
            (handler task))))

(defmethod display-name ((task task))
  (format nil "~{~a~^-~}" (alexandria:ensure-list (id task))))

(defmethod initialize-instance :after ((task task) &rest initargs)
  (declare (ignore initargs))
  (setf (lock task) (bt2:make-lock :name (display-name task))
        (gethash (id task) *tasks*) task))

(defun make-task (id handler)
  (let ((old-task (gethash id *tasks*)))
    (cond
      (old-task
       (setf (handler old-task) handler)
       old-task)
      (t (make-instance 'task :id id :handler handler)))))



(defun unregister-recurring-task (task)
  (cl-cron:delete-cron-job task)
  (remhash (id task) *tasks*))

(defun invoke-task (task &key lock-held-p (aquire-lock-timeout 0.001))
  (tagbody
   :retry
     (restart-case
         (if lock-held-p
             (bt2:with-lock-held ((lock task) :timeout aquire-lock-timeout)
               (funcall (handler task)))
             (funcall (handler task)))
       (pause-task ()
         :report "Remove the task from cron and abort."
         (cl-cron:delete-cron-job task)
         (abort))
       (unregister-and-abort ()
         :report "Unregister the task and abort."
         (unregister-recurring-task task)
         (abort))
       (retry-handler ()
         :report "Retry calling the task's handler"
         (go :retry)))))

(defun register-recurring-task (id handler &key schedule run-now-p)
  (check-type id (or symbol list))
  (let* ((task (make-task id handler))
         (display-name (display-name task)))
    (labels ((invoke ()
               (bt:make-thread (lambda ()
                                 (invoke-task task :lock-held-p t))
                               :name display-name)))
      (apply #'cl-cron:make-cron-job
             #'invoke
             :hash-key task
             schedule)
      (when run-now-p (invoke)))
    task))

(defmacro define-recurring-task ((id &key run-now-p) &body body)
  (alexandria:once-only (id)
    `(cache-cache.task:register-recurring-task
      ,id
      (lambda () ,@body)
      :run-now-p ,run-now-p)))
