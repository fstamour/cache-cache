(in-package #:cache-cache)

;; For interactive debugging
(setf h:*catch-errors-p* nil)
(setf h:*catch-errors-p* t)

(log:config :debug)



(in-package #:cache-cache.gitlab.source)

(source-by-id 1)

(initialize-topic (source-by-id 1) :project)

(initialize-topic (source-by-id 1) :issue)

(setf (items (source-by-id 1) :issue) nil)


;;; Caching

(cache-filename (source-by-id 1) :issue)
;; => "1-issue"

(cache-pathname (source-by-id 1) :issue)
;; => #P"/home/fstamour/.cache/cache-cache/1-issue.sbin"


(write-cache-file (source-by-id 1) :project)
(write-cache-file (source-by-id 1) :issue)


(read-cache-file (source-by-id 1) :project)

(time
 (read-cache-file (source-by-id 1) :issue))
;; 0.623 seconds of real time
;; 0.622624 seconds of total run time (0.614839 user, 0.007785 system)
;; [ Real times consist of 0.044 seconds GC time, and 0.579 seconds non-GC time. ]
;; [ Run times consist of 0.041 seconds GC time, and 0.582 seconds non-GC time. ]
;; 100.00% CPU
;; 2,190,069,700 processor cycles
;; 257,261,856 bytes consed


;;; Jobs/Tasks

(setf cl-cron::*cron-dispatcher-thread* nil)
(cl-cron:start-cron)
(cl-cron:stop-cron)

cl-cron::*cron-jobs-hash*

(in-package #:cache-cache.gitlab.client)

(let ((source (cache-cache::source-by-id 1))
      (topic :issue))
  (cache-cache.task:define-recurring-task
      (`((:source-id ,(id source)) (refresh ,topic)) :run-now-p t)
    (refresh-topic source topic)))


;;; Searching

(in-package #:cache-cache.search)

(trace search-source)
(trace search-in-list)
(trace narrow-down-using-index)
(trace cache-cache.gitlab.search::handler/search)
(untrace)

(let* ((index (first (alexandria:hash-table-values *indexes*)))
       (table (table index))
       (query "de"))
  (search-in-list query (alexandria:hash-table-keys table)
                  :test #'string=))

(search-source (cache-cache::source-by-id 1) "key" :limit 50)

(in-package #:cache-cache.gitlab.search)

(import 'cache-cache::source-by-id)

(time (find-issues "clean" (source-by-id 1)))
(time (find-issues "de" (source-by-id 1)))
(time (find-issues "dep" (source-by-id 1)))
(time (find-issues "deploy" (source-by-id 1)))

;; Searching issues by content

(defparameter *matches*
  (find-by "bug"
           (source-by-id 1) :issue
           #'cache-cache.gitlab.client::item-description))

(length *matches*)
;; => 3043 occurences of "bug"... fun...

(mapcar #'item-title *matches*)

(item-references/full (first (a:hash-table-values (items (source-by-id 1) :issue))))


;;; Inspecting

(in-package #:cache-cache.gitlab.search)

;; /projects/:id/issues/:issue_iid/discussions

(cache-cache.generic:supported-topics
 (source-by-id 1))

(source-by-id 1)

(initialize (source-by-id 1))

(initialize-topic (source-by-id 1) :issue)

;; get-all-issues, but "incremental"
(let ((source (source-by-id 1)))
  (cache-cache.gitlab.client::get-all-issues
   source
   (lambda (issues)
     (cache-cache::by-id (cache-cache.gitlab.client::remove-moved-issues issues)
                         (items source :issue)))))

(statistics (source-by-id 1))


;; collect all topics
(uiop:while-collecting (collect)
  (map-supported-topics
   (lambda (source topic)
     (collect (items source topic)))
   (source-by-id 1)))

;;;;;;;;;;; Stuff below this is probably out of date



;;; Group labels to terraform...

;; Get all the root groups' labels
(defvar *group-labels*
  (http-request-get-all
   (format nil
           "~a/groups/~a/labels?per_page=1000"
           *base-uri*
           *root-group-id*)))

;; Deduplicate the colors
(defvar *color* (make-hash-table :test 'equal))

;; Create a terraform file with one "local" per color
(alexandria:with-output-to-file (stream "./label-colors.tf"
                                        :if-exists :supersede)
  (format stream "locals {~%")
  (loop :for color :in
                   (sort (copy-seq
                          (a:hash-table-keys
                           (by *group-labels* :key (lambda (label)
                                                     (gethash "color" label)))))
                         #'string<)
        :for i :from 0
        :do
           (setf (gethash color *color*) i)
           (format stream "  color~d = ~s~%" i color))
  (format stream "}"))



(defun sanitize-string (string)
  (str:replace-using
   `("\\" "\\\\"
          ,(string #\newline) "\\n"
          "\"" "\\\"")
   (str:trim string)))

;; Generate a terraform file that contains all the group labels
(alexandria:with-output-to-file (stream "./labels.tf"
                                        :if-exists :supersede)
  (loop
    ;; set importp to t to also generate the import blocks (useful the
    ;; first time)
    :with importp = nil
    :for labels-by-name = (by *group-labels* :key 'item-name)
    :for name :across (sort
                       (map 'vector (lambda (label) (gethash "name" label))
                            *group-labels*)
                       #'string<)
    :for label = (gethash name labels-by-name)
    :for id = (item-id label)
    :for resource-name = (format nil "label~a" id)
    :do
       (format stream "~{~?~}"
               `(,@ (when importp "
import {
  id = \"~a:~a\"
  to = gitlab_group_label.~a
}
"
                          ,(list *root-group-id* id resource-name))
                    "
resource \"gitlab_group_label\" ~s {
   group       = ~s
   name        = ~s
   description = \"~a\"
   color       = local.color~d
}
"
                    ,(list
                      resource-name *root-group-id*
                      (gethash "name" label)
                      ;; TODO use sanitize-string
                      (str:replace-using
                       `("\\" "\\\\"
                              ,(string #\newline) "\\n"
                              "\"" "\\\"")
                       (gethash "description" label))
                      ;; (gethash "color" label)
                      (gethash (gethash "color" label) *color*)
                      )))))

;; IMO, it took too much time to plan with so many labels, so I move
;; these resources out into another (new) terraform state).
;;
;; Use this to remove them from the state (this takes forever, it might
;; have been faster (but less safe) to pull the state, edit and push).
;; 1. terraform state list | grep gitlab_group_label | xargs -n 1 echo tf state rm > remove-labels-from-state.sh
;; 2. < review remove-labels-from-state.sh >
;; 3. sh remove-labels-from-state.sh


;;; Getting a project's environements' ids (also to import into terraform)


(let ((project-id *the-project-id*))
  (defparameter *environments*
    (http-request-get-all
     (format nil
             "~a/projects/~a/environments"
             *base-uri*
             project-id))
    ))

;; Show the environments as json, but keeping only the "id" and "name"
;; attributes.
#++
(jzon:stringify
 *environments*
 :pretty t
 :replacer (lambda (k v)
             (if (and k (stringp k))
                 (and (member k '("id" "name" "external_url") :test #'string=) t)
                 t)))

;; Make an import terraform block for the environments
(loop :for env :across *environments*
      :do
         (format t "~%import {
  id = ~s
  to = gitlab_project_environment.apigateway[~s]
}"
                 (item-id env)
                 (item-name env)))


;;; Getting all epics

;; TODO updated_after
(defparameter *epics*
  (http-request-get-all
   (format nil
           "~a/groups/~a/epics?per_page=1000"
           *base-uri*
           *root-group-id*)))

(length *epics*)
(hash-table-count *epics*)
(aref *epics* 0)


;;; Trying to fetch my activities...

;; It seems to always return the last 7 days instead of respecting the
;; :after and :before...
(progn
  (format t "~%~%==================~%~%")
  (loop :for event :across (http-request-get-all
                            (puri:render-uri
                             (make-uri "/api/v4/events"
                                       (format-query
                                        :after "2023-07-04"
                                        :before "2023-07-07"
                                        :scope :all))
                             nil))
        :do (format t "~%~%~a~%"
                    (alexandria:hash-table-alist event))))


;;; Trying to use serapeum to generate the documentation

;; (asdf:load-system 'serapeum/docs)
;; I had some issues with guix... lol
(load
 (merge-pathnames
  "docs.lisp"
  (asdf:system-source-directory 'serapeum)))

(let ((root (asdf:system-source-directory 'cache-cache))
      (home (user-homedir-pathname)))
  (if (uiop:absolute-pathname-p (enough-namestring root home))
      (error "The system's location ~s is not in the user's home directory (~s), did you load the system from guix or nix?"
             root home)

      (serapeum.docs:update-function-reference
       ;; (namestring (uiop:merge-pathnames* root "REFERENCE.md"))
       "REFERENCE.md"
       :cache-cache)))

;; It failed miserably because swank wasn't able to locate the source
;; of a few symbols. I tried to modify serapeum/docs' code, but too
;; many things assume that source information is available.



;;; "hot-loading" of common lisp systems installed in a guix profile
;;;
;;; I added a guix package definition for my project breeze and
;;; installed it in a profile. I wanted to load it without restarting
;;; the lisp image. I managed to do it by adding the path to the
;;; profile to the enviroment variable XDG_CONFIG_DIRS and calling
;;; (asdf:clear-source-registry). This works because guix's sbcl is
;;; compiled with a patched asdf that looks into all directories in
;;; XDG_CONFIG_DIRS for its configurations.

(defvar *xdg-config-dirs*
  (uiop:getenv "XDG_CONFIG_DIRS"))
"The original value of the environment variable XDG_CONFIG_DIRS"

;; Update the environment variable
(setf (uiop:getenv
       "XDG_CONFIG_DIRS")
      (format nil "~a:~a"
              *xdg-config-dirs*
              (merge-pathnames
               "dev/guix-configurations/lisp-profile/etc"
               (user-homedir-pathname))))


;; Check the value
(uiop:getenv "XDG_CONFIG_DIRS")

;; Restore the original value
(setf (uiop:getenv "XDG_CONFIG_DIRS") *xdg-config-dirs*)

(setf (uiop:getenv "XDG_CONFIG_DIRS") "/gnu/store/gb2rib2s0rqnc60dhmv44h8rvw34bawv-profile/etc:/home/fstamour/.guix-home/profile/etc:/home/fstamour/.guix-home/profile/etc:/home/fstamour/.guix-home/profile/etc:/home/fstamour/.guix-home/profile/etc/xdg:/home/fstamour/.guix-home/profile/etc:/etc/xdg/xdg-stumpwm:/home/fstamour/.guix-home/profile/etc:/etc/xdg")

;; Try to find the system
(asdf:locate-system 'breeze)

;; Try to reload the asdf's configurations
(asdf:clear-source-registry)

;; Try to load the system
(asdf:load-system :breeze :force :all)

;; TODO check if we're on guix, of if guix is available at least
;; TODO ensure that path to profile is in XDG_CONFIG_DIRS
;; TODO locate-system
;; TODO guix install in profile, then clear-source-registry, locate again

(asdf:load-system :flute)

;; ./guix install cl-mcclim -p lisp-profile

(asdf:load-system :mcclim)

(asdf:map-systems #'print)

(asdf/system-registry:clear-registered-systems)




(clear-cache (source-by-id 1) :issue)


;; Find issue(s) by name (exact match)
(let ((source (source-by-id 1)))
  (loop
    ;; iterate over all issues
    :for iid
      :being :the hash-key :of (items source :issue)
        :using (hash-value issue)
    :when (string= (item-title issue)
                   "This is the exact issue title to search for")
      :collect issue))



;;; GitLab WIP stuff (comments, labels, some mutation too!)

;; Rename a label
(http-request-gitlab uri source
                     :content-type "application/json"
                     :content (jzon:stringify
                               (a:plist-hash-table `(:new_name ,new-name)))
                     :method :put)

;; Getting comments on issues

(defparameter *notes* (make-hash-table))

;; I don't want to get the comments on every issues
(defparameter *candidate-issues*
  (loop
    :with source = (source-by-id 1)
    ;; iterate over all issues
    :for iid :being :the hash-key :of (items source :issue)
      :using (hash-value issue)
    ;; filter out issues updated before 2023-11-07)
    :when (cache-cache.gitlab.search::timestamp-string<
           "2023-12-00T00:00:00.000000+00:00"
           (item-updated-at issue))
      :collect issue))

(time
 (loop
   :with source = (source-by-id 1)
   :for issue :in *candidate-issues*
   :for iid = (item-iid issue)
   :for i :from 1
   :for uri = (cache-cache::make-uri
               (api-url source)
               "projects/"
               (cache-cache::ensure/ (item-project-id issue))
               "issues/"
               (cache-cache::ensure/ iid)
               "notes"
               #++
               (cache-cache::format-query
                :per-page 5
                :include-subgroups t
                :order-by :updated-at))
   :for notes = (ignore-errors
                 (http-request-get-all
                  (puri:render-uri uri nil)
                  (token source)
                  (lambda (notes)
                    (cache-cache::by-id notes
                                        *notes*))))
   :do (print i)
   #++ (:when notes
         :collect (list
                   (puri:render-uri uri nil)
                   notes))))


;; Filtering notes

(defun item-author-name (item)
  (a:when-let* ((author (gethash "author" item))
                (name (gethash "name" author)))
    name))

(defun item-author-name= (item author-name)
  (a:when-let* ((author (gethash "author" item))
                (name (gethash "name" author)))
    (string= author-name name)))

(defparameter *filtered-notes*
  (loop
    :with source = (source-by-id 1)
    ;; iterate over all issues
    :for iid :being :the hash-key :of (items source :note)
      :using (hash-value note)
    ;; filter out issues updated before 2023-11-07)
    :for body = (gethash "body" note)
    :when (and
           (not (gethash "system" note))
           (cache-cache.gitlab.search::timestamp-string<
            "2023-10-00T00:00:00.000000+00:00"
            (item-created-at note)
            "2023-12-00T00:00:00.000000+00:00")
           (item-author-name= note "Not Me")
           ;; Keep onlyt the notes that start with a mention
           (a:starts-with #\@ body))
      :collect note))

;; Caching notes to file (ad-hoc)
(cache-cache.cache::write-hash-table
 (cache-pathname (source-by-id 1) :note)
 *notes*)

;; This works nicely
(read-cache (source-by-id 1) :note)

;; DELETE /projects/:id/issues/:issue_iid/notes/:note_id
(defun delete-note (source note)
  (let ((uri (cache-cache::make-uri
              (api-url source)
              "projects/"
              (cache-cache::ensure/ (item-project-id note))
              "issues/"
              (cache-cache::ensure/ (gethash "noteable_iid" note))
              "notes/"
              (item-id note))))
    (multiple-value-bind
          (body status-code headers uri-object stream must-close reason-phrase)
        (drakma:http-request
         (puri:render-uri uri nil)
         :method :delete
         :additional-headers (list (token-header (token source))))
      (when body (jzon:parse body)))))


(let ((source (source-by-id 1)))
  (mapcar #'(lambda (note)
              (delete-note source note)) *filtered-notes*))
