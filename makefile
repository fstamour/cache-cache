
.PHONY: run-dev
run-dev:
	# Assuming everything is loaded by direnv
	sbcl \
		--eval "(ql:quickload '(#:cache-cache #:swank))" \
		--eval "(swank:create-server :port (find-port:find-port :min 4005) :dont-close t)" \
		--eval "(cache-cache:serve :join-thread-p t)"

.PHONY: help
help:
	@echo 'Default target is   run-dev'
	@echo ''
	@echo 'Available targets:'
	@echo '  help  	Show this help.'
	@echo '  run-dev	Load the system in sbcl, start a swank server and the web server.'
