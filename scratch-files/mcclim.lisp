;;;; CC stands for cache-cache

(in-package :common-lisp-user)

;; (asdf:load-system :mcclim :force t)
;; (asdf:load-system :mcclim)

(defpackage #:cc.gui
  (:use :clim :clim-lisp)
  (:export "main"))

(in-package :cc.gui)


(defun make-debouncer (delay-in-seconds callback)
  ;; TODO there's probably a bunch of race conditions...
  (let ((last-time)
        (thread)
        (saved-args)
        (lock (bt:make-lock (format t "debounced lock ~s" callback))))
    (labels ((now ()
               (/ (get-internal-real-time)
                  #.(float internal-time-units-per-second)))
             (update-last-time (now)
               (setf last-time now))
             (overduep (now)
               (and last-time
                    (<= delay-in-seconds (- now last-time))))
             (actually-call (now args)
               (update-last-time now)
               (setf thread nil)
               (apply callback args))
             (wait-loop ()
               (loop
                 :for now = (now)
                 :until (overduep now)
                 :do (let ((delay (max 0.025 (- (+ last-time delay-in-seconds) now))))
                       #++ (progn (format *debug-io* "~&About to sleep ~s seconds" delay)
                                  (force-output *debug-io*))
                       (sleep delay)))
               (bt:with-lock-held (lock)
                 (actually-call (now) saved-args)))
             (maybe-funcall (&rest args)
               (update-last-time (now))
               (setf saved-args args)
               (unless thread
                 (setf thread (bt:make-thread #'wait-loop
                                              :name "Debouncer thread")))
               last-time))
      #'maybe-funcall)))


#++ ;; testing make-debouncer
(flet ()
  (let ((hi (make-debouncer
             0.5
             (lambda ()
               (format *debug-io* "~&hullo ~a~%" (get-internal-real-time))
               (force-output *debug-io*)))))
    (funcall hi)
    (funcall hi)
    (sleep 0.1)
    (funcall hi)
    (sleep 0.5)
    (funcall hi)
    (funcall hi)
    (sleep 1)
    (funcall hi)
    hi))

#|

Idea: I have code that supports incremental search (narrowing), I
should be able to incrementally show the results too, instead of
clearing everything and starting from scratch for every keypress.

The debouncing business might not even be needed if everything is fast enough.

|#

(defun new-query-callback (application-frame value-pane new-value)
  (declare (ignore value-pane))
  (let* ((*application-frame* application-frame)
         (output-pane (find-pane-named application-frame 'app))
         (*standard-output* output-pane))
    ;; Clear the pane
    (window-clear output-pane)
    ;; Print the query
    (format t "Query: ~s" new-value)
    (force-output)
    ;; Print the results
    ;; TODO use execute-frame-command to run this in a thread-safe(r) way

    #++
    ;; This was just to test the "rendering speed"
    (time
     (with-output-buffered (*output-pane*)
       (loop :for i :upto 1000
             :do (format t "~%~a" i))
       (force-output)))

    #++
    ;; If the query is too short, there's too many results, but this
    ;; might not be an issue anymore with the buffering.
    (when (< 4 (length new-value))
      (let ((matches
              (uiop:while-collecting (matches)
                (loop
                  :with query = new-value
                  :with now = (get-internal-real-time)
                  :for source :in cache-cache::*sources*
                  :do
                  #++ ;; TODO Add a callback to search-source
                   (cache-cache.cache::search-source source query :limit 50 :topic topic)
                   (cache-cache.gitlab.search::hack/search
                    query source
                    (lambda (topic item)
                      (declare (ignore topic))
                      (matches item))
                    #++ (lambda (topic item)
                          (incf count)
                          (format *debug-io* "~& ~a: ~a seconds"
                                  count
                                  (float (/ (- (get-internal-real-time) now)
                                            internal-time-units-per-second)))
                          ;; printing the results as they come was too slow :/
                          (format t "~%~(~a~) ~s" topic item)))))))
        (format t "~&~a match" (length matches))
        (with-output-buffered (*output-pane*)
          (formatting-item-list ()
            (loop
              :for item :in matches
              :do
                 (formatting-cell ()
                   (with-output-as-gadget (*standard-output*)
                     (make-pane 'push-button :label (or (cache-cache.gitlab.client::item-title item)
                                                        (prin1-to-string item))))))))
        ;; (format t "~{~&~a~}" matches)
        (force-output)))))

;; The time it takes to search is not the issue
#++
(let ((query "deploy"))
  (loop
    :with now = (get-internal-real-time)
    :with count = 0
    :for source :in cache-cache::*sources*
    :do
       (cache-cache.gitlab.search::hack/search
        query source
        (lambda (topic item)
          (incf count)
          (format *debug-io* "~& ~a: ~a seconds"
                  count
                  (float (/ (- (get-internal-real-time) now)
                            internal-time-units-per-second)))
          ;; printing the results as they come was too slow :/
          #++ (format t "~&~(~a~) ~s" topic item)))))


(defun new-query-value-callback (value-pane new-value)
  (format *debug-io* "~%out: ~s" *standard-output*)
  (format *debug-io* "~%value-pane: ~s" value-pane)
  (format *debug-io* "~%*application-frame*: ~s" *application-frame*)
  ;; "Testing" the rendering speed
  (alexandria:when-let ((pane (find-pane-named *application-frame* 'app)))
    (format *debug-io* "~%app pane: ~s" pane)
    (window-clear pane)
    ;; This was just to test the "rendering speed"
    (time
     (with-output-buffered (pane)
       (loop :for i :upto 1000
             :do (format pane "~%~a" i))
       ;; (force-output pane)
       )))
  #++
  (let ((callback (slot-value *application-frame* 'new-query-callback)))
    ;; (format *debug-io* "~%out: ~s" *standard-output*)
    (funcall callback *application-frame* value-pane new-value)))

(define-application-frame cc ()
  ((new-query-callback
    :initform (make-debouncer 0.25 'new-query-callback)))
  (:pointer-documentation t)
  (:pretty-name "Cache-cache")
  (:panes
   (query text-field
          :value-changed-callback 'new-query-value-callback)
   (app :application
        :display-time nil
        ;; :end-of-line-action :allow
        :end-of-page-action :allow)
   (int :interactor))
  (:layouts
   (default (vertically ()
              query
              (:fill app)
              (1/5 int))))
  (:reinitialize-frames t))

(define-cc-command (command-quit :name "Quit") ()
  (frame-exit *application-frame*))

(defun main ()
  (run-frame-top-level (make-application-frame 'cc)))

#++
(main)
