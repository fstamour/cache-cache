(in-package :cc.gui)

(main)

(defparameter *cc-frame*
  (find-application-frame 'cc
                          :create nil
                          :activate nil))
(defparameter *output-pane*
  (find-pane-named *cc-frame* 'app))

(frame-manager-frames (find-frame-manager))

(stream-output-history *output-pane*)

;; (map-over-output-records )

(clear-output-record
 (stream-output-history *output-pane*))

;; (redisplay-frame-pane *cc-frame* *output-pane*)

(redisplay-frame-panes *cc-frame*)

(window-clear *output-pane*)

;; NOP (stream-clear-output *output-pane*)

;; Don't do that:
;; (setf (stream-current-output-record *output-pane*) nil)

(progn
  (with-output-buffered (*output-pane*)
    (window-clear *output-pane*)
    (loop :for i :upto 30
          :do (format *output-pane* "~%~a" i))
    (force-output *output-pane*)))


(frame-panes *application-frame*)
;; map-over-sheets


(let ((*standard-output* (find-pane-named *actual-application-frame* 'app)))
  (format t "~&hola")
  (force-output))


(force-output
 (find-pane-named *actual-application-frame* 'app))


(pane-frame (find-pane-named *actual-application-frame* 'app))

;; *default-frame-manager*
(frame-manager-frames (find-frame-manager))
;; => (#<CC {1015A68403}>)

;; *application-frame*

#|
Ideas for commands/presentations:

- copy a project's id
- clone a project
- see a project's issues (e.g. narrow the searches)
- keep track of an MR's pipelines?

|#




(define-cc-command (command-parity :name "Parity") ((number 'integer))
  (format t "~a is ~a~%" number
          (if (oddp number) "odd" "even")))


(define-cc-command (command-sources :name "Sources") ()
  (format t "~&Sources: ~a" cache-cache::*sources*)
  (force-output))


(define-cc-command (command-stats :name "Statistics") ()
  (format t "~&Statistics:")
  ;; TODO Don't I have a "map-sources"?
  (loop
    :for source :in cache-cache::*sources*
    :do
       (loop
         :for (topic number) :in (cache-cache:statistics source)
         :do (format t "~%  - ~a ~a" number topic)))
  (force-output))


(define-presentation-type gitlab-issue ())
